package com.example.sqliteclass;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

public class Model {
    public SQLiteDatabase getCon(Context context){
        SQLiteConnect connect = new SQLiteConnect(context,"dbuser", null,1);
        SQLiteDatabase database = connect.getWritableDatabase();
        return database;
    }

    int insertUser(Context context, UserDTO userDTO){
        int response = 0;
        String sqlQueryInsert = "INSERT INTO user (name, email, phone) VALUES ('"+userDTO.getName()+"','"+userDTO.getEmail()+"','"+userDTO.getPhone()+"')";
        SQLiteDatabase database = this.getCon(context);
        try {
            database.execSQL(sqlQueryInsert);
            response = 1;
        }catch (Exception err){
            Log.w("Model", "insertUser: catch => " + err );
        }

        return response;
    }
}
