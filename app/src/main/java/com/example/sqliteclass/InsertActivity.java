package com.example.sqliteclass;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class InsertActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_insert);

        Button btnSave = findViewById(R.id.btnSave);
        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

               EditText etName = findViewById(R.id.etName);
               EditText etEmail = findViewById(R.id.etEmail);
               EditText etPhone = findViewById(R.id.etPhone);
               Model dbModel = new Model();
               UserDTO userDTO = new UserDTO();

               userDTO.setName(etName.getText().toString());
               userDTO.setEmail(etEmail.getText().toString());
               userDTO.setPhone(etPhone.getText().toString());

               int responseInsert = dbModel.insertUser(InsertActivity.this, userDTO);
               if(responseInsert == 0){
                   Toast.makeText(InsertActivity.this,"Error al crear usuario", Toast.LENGTH_LONG).show();
               } else {
                   Toast.makeText(InsertActivity.this,"Usuario creado", Toast.LENGTH_LONG).show();
               }
            }
        });
    }
}